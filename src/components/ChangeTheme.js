import React from 'react'

class ChangeTheme extends React.Component {

  render() {
    return (
      <div className="switch-theme">
        <button className="btn btn-default" onClick={() => this.props.onClick()}>
          Change theme
        </button>
      </div>
    )
  }
}

export default ChangeTheme;