import React from 'react'

class Skills extends React.Component {
  render() {
    return (
      <section id="skills" className="skills">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-6 z-i">
              <h3 className="skills-h3 wow fadeInLeft" data-wow-delay="0.3s">skillset</h3>
              <h1 className=" wow fadeInLeft" data-wow-delay="0.6s">I've Got Some Skills.</h1>
              <p className="skills-p wow fadeInUp" data-wow-delay="0.3s">Lorem ipsum Elit ut consequat veniam eu nulla
                nulla reprehenderit reprehenderit
                sit velit
                in cupidatat ex aliquip ut cupidatat Excepteur tempor id irure sed dolore sint sunt voluptate
                ullamco nulla qui Duis qui.</p>
              <ul className="skill-bars">
                <li className="wow fadeInLeft" data-wow-delay="0.2s">
                  <p>HTML</p>
                  <div className="progress percent90">
                    <span>90%</span>
                  </div>
                </li>
                <li className="wow fadeInLeft" data-wow-delay="0.2s">
                  <p>CSS</p>
                  <div className="progress percent85">
                    <span>85%</span>
                  </div>
                </li>
                <li className="wow fadeInLeft" data-wow-delay="0.2s">
                  <p>JQUERY</p>
                  <div className="progress percent70">
                    <span>70%</span>
                  </div>
                </li>
                <li className="wow fadeInLeft" data-wow-delay="0.2s">
                  <p>BOOTSTRAP</p>
                  <div className="progress percent70">
                    <span>70%</span>
                  </div>
                </li>
                <li className="wow fadeInLeft" data-wow-delay="0.2s">
                  <p>ANGULAR</p>
                  <div className="progress percent55">
                    <span>55%</span>
                  </div>
                </li>
                <li className="wow fadeInLeft" data-wow-delay="0.2s">
                  <p>REACT JS</p>
                  <div className="progress percent50">
                    <span>50%</span>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

export default Skills;