import React from 'react';
import Header from './header/Header'
import Home from './home/Home'
import About from './about/About'
import Skills from './skills/Skills'
import ChangeTheme from './ChangeTheme'
import Api from './api/Api'
import Contact from './contact/Contact'
import WOW from '../wow'

class Main extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isDark: true,
      classOverlay: 'overlay-off',
      textColor: 'light'
    }
  }

  componentDidMount() {
    new WOW.WOW(
      {
        boxClass: 'wow',      // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset: 70,          // distance to the element when triggering the animation (default is 0)
        mobile: true,       // trigger animations on mobile devices (default is true)
        live: false,       // act on asynchronously loaded content (default is true)
        callback: function (box) {
          // the callback is fired every time an animation is started
          // the argument that is passed in is the DOM node being animated
        },
        scrollContainer: null // optional scroll container selector, otherwise use window
      }
    ).init();
  }

  changeTheme() {
    let overlay = this.state.isDark ? 'overlay-on' : 'overlay-off';
    let color = this.state.isDark ? 'dark' : 'light';
    this.setState({
      isDark: !this.state.isDark,
      classOverlay: overlay,
      textColor: color
    })
  }

  render() {
    return (
      <div id="main" className={this.state.textColor}>
        <div className={this.state.classOverlay}></div>
        <Header/>
        <Home/>
        <About/>
        <Skills/>
        <ChangeTheme onClick={() => this.changeTheme()}/>
        <Api/>
        <Contact/>
      </div>
    )
  }
}

export default Main