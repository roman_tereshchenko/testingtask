import React from 'react'

class About extends React.Component {
  render() {
    return (
      <section id={'about'}>
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-6 about-left">
              <div className="row justify-content-end pb-4">
                <div className="col-md-9 col-xs-12">
                  <div className="row justify-content-end">
                    <div className="about-profile">
                      <div className="intro wow fadeInUp" data-wow-delay="0.2s">
                        <h3 className="red">about</h3>
                        <h1>More About Me</h1>
                        <p>
                          Lorem ipsum Ut eiusmod ex magna sit dolor esse adipisicing minim ad cupidatat eu veniam
                          nostrud mollit laboris sunt magna velit culpa consectetur nostrud consectetur labore sed do.
                        </p>
                      </div>
                      <p className="wow fadeInUp" data-wow-delay="0.1s">
                        Lorem ipsum Nisi officia Duis irure voluptate dolor commodo pariatur occaecat aliquip
                        adipisicing voluptate Ut in qui ea sint occaecat in commodo in in in incididunt ut sunt in Ut
                        Duis in ut ex qui anim cupidatat cupidatat ex in non dolore labore ea amet cillum ea qui dolor
                        nisi sed velit mollit exercitation ex fugiat labore in deserunt culpa laborum culpa anim dolore
                        laboris amet irure mollit proident velit fugiat aute ea elit magna consequat qui officia quis
                        elit Duis dolor esse cupidatat tempor proident voluptate aliqua ex cupidatat do eiusmod veniam
                        irure laborum ut magna nostrud dolore ullamco commodo elit sit magna aliqua laborum veniam
                        officia dolor.
                      </p>
                      <p className="wow fadeInUp" data-wow-delay="0.1s">
                        Lorem ipsum Minim nisi dolore proident Ut dolor quis deserunt voluptate Ut voluptate enim ut
                        adipisicing fugiat cupidatat nisi adipisicing labore non adipisicing in sit proident. Ut in qui
                        ea sint occaecat in commodo in in in incididunt ut
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-6 about-right">
              <div className="row justify-content-start pb-4">
                <div className="col-md-9 col-xs-12">
                  <div className="row justify-content-start">
                    <div className="about-education">
                      <div className="intro">
                        <h3 className="red wow fadeInUp" data-wow-delay="0.4s">education</h3>
                        <h1 className="wow fadeInUp" data-wow-delay="0.3s">My Education</h1>
                        <div className="university">
                          <div className="university-header wow fadeInUp" data-wow-delay="0.2s">
                            <p className="university-time">Sept 2007 - June 2011</p>
                            <h3 className="university-name">Kirovograd Cybernetic-Technical College</h3>
                            <h4 className="university-degree">Associate Degree</h4>
                          </div>
                          <div className="university-content wow fadeInRight" data-wow-delay="0.7s">
                            <p className="university-info">
                              Lorem ipsum Occaecat do esse ex et dolor culpa nisi ex in magna consectetur nisi cupidatat
                              laboris esse eiusmod deserunt aute do quis velit esse sed Ut proident cupidatat nulla esse
                              cillum laborum occaecat nostrud sit dolor incididunt amet est occaecat nisi.
                            </p>
                          </div>
                        </div>
                        <div className="university">
                          <div className="university-header wow fadeInUp" data-wow-delay="0.2s">
                            <p className="university-time">Sept 2011 - Jule 2015</p>
                            <h3 className="university-name">Kirovohrad National Technical University</h3>
                            <h4 className="university-degree">Bachelors Degree</h4>
                          </div>
                          <div className="university-content wow fadeInRight" data-wow-delay="0.7s">
                            <p className="university-info">
                              Lorem ipsum Occaecat do esse ex et dolor culpa nisi ex in magna consectetur nisi cupidatat
                              laboris esse eiusmod deserunt aute do quis velit esse sed Ut proident cupidatat nulla esse
                              cillum laborum occaecat nostrud sit dolor incididunt.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

export default About