import React from 'react'

class Contact extends React.Component {
  render() {
    return (
      <footer id="contact" className="d-flex align-items-center">
        <div className="container-fluid">
          <div className="row justify-content-center">
            <div className="col-md-10 z-i text-center">
              <div className="row flex-column wow fadeInUp" data-wow-delay=".7s">
                <h3>CONTACT</h3>
                <h1>Feel Free to Contact Me.</h1>
              </div>
              <div className="row contact-details wow fadeInLeft" data-wow-delay=".3s">
                <div className="col-md-4 p-0">
                  <h5>Address</h5>
                  <p className="wow fadeInUp" data-wow-delay=".7s">Ukraine, Kropyvnytskyi</p>
                </div>
                <div className="col-md-4 p-0">
                  <h5>Email</h5>
                  <p className="wow fadeInUp" data-wow-delay=".8s">tereshchenkoroma92@gmail.com</p>
                </div>
                <div className="col-md-4 p-0">
                  <h5>Phone Number</h5>
                  <p className="wow fadeInUp" data-wow-delay=".9s">Mobile: (+38) 099 159 1851</p>
                </div>
              </div>
              <div className="row justify-content-center">
                <ul className="contact-social">
                  <li className="wow fadeInUp" data-wow-delay=".3s">
                    <a className="header-a" href="https://www.facebook.com/" target="_blank" rel="noopener noreferrer">
                      Facebook
                    </a>
                  </li>
                  <li className="wow fadeInUp" data-wow-delay=".4s">
                    <a className="header-a" href="https://twitter.com" target="_blank" rel="noopener noreferrer">
                      Twitter
                    </a>
                  </li>
                  <li className="wow fadeInUp" data-wow-delay=".5s">
                    <a className="header-a" href="https://telegram.org" target="_blank" rel="noopener noreferrer">
                      Telegram
                    </a>
                  </li>
                  <li className="wow fadeInUp" data-wow-delay=".6s">
                    <a className="header-a" href="https://vk.com" target="_blank" rel="noopener noreferrer">
                      Vk
                    </a>
                  </li>
                  <li className="wow fadeInUp" data-wow-delay=".7s">
                    <a className="header-a" href="https://www.instagram.com" target="_blank" rel="noopener noreferrer">
                      Instagram
                    </a>
                  </li>
                </ul>
              </div>
              <div className="row justify-content-center wow fadeInUp" data-wow-delay="1s">
                <span className="copy-right">© Copyright RomanT 2018.</span>
              </div>
            </div>
          </div>
        </div>
      </footer>
    )
  }
}

export default Contact