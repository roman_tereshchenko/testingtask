import React from 'react'

class Menu extends React.Component {

  handleClick = () => setTimeout(this.props.onClick, 1000);

    render() {
      return (
        <div className={this.props.res + 'navbar-collapse'} id="navbarNavDropdown">
          <a className="close-button" title="close"
             onClick={() => this.props.onClick()}><span>Close</span></a>
          <h3>Roman.</h3>
          <ul className="navbar-nav">
            <li className="nav-item">
              <a className="nav-link" href="#home" onClick={this.handleClick}>Home</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#about" onClick={this.handleClick}>About</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#skills" onClick={this.handleClick}>Skills</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#api" onClick={this.handleClick}>Api</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#contact" onClick={this.handleClick}>Contact</a>
            </li>
          </ul>
        </div>
      )
    }
}

export default Menu