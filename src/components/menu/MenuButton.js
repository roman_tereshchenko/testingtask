import React from 'react'

class MenuButton extends React.Component {

  handleClick = this.props.onClick;

  render() {
    return (
      <div>
        <button className="btn menu-button d-flex" onClick={this.handleClick} type="button">
          <span className="menu-button-text">Menu</span>
          <span className="menu-icon"></span>
        </button>
      </div>
    )
  }
}

export default MenuButton