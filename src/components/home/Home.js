import React from 'react'

class Home extends React.Component {
  render() {
    return (
      <section id={'home'}>
        <div className='container-fluid'>
          <div className="row">
            <div className="col-md-10 z-i">
              <div className="home-content wow fadeInLeft" data-wow-duration="1.2s" data-wow-delay="0.7s">
                <h3>Hi there!</h3>
                <h1>
                  I am Roman Tereshchenko. <br/>
                  Web Developer <br/>
                  Based in Kropyvnytskyi Ukraine.
                </h1>
                <ul className={'home-section-links'}>
                  <li className="wow fadeInUp" data-wow-delay="1.1s">More <a className="header-a" href="#about">About
                    Me</a></li>
                  <li className="wow fadeInUp" data-wow-delay="1.3s">View <a className="header-a" href="#skills">My
                    Skills</a></li>
                  <li className="wow fadeInUp" data-wow-delay="1.5s">Contact <a className="header-a" href="#contact">My
                    Email</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

export default Home