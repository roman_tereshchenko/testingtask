import React from 'react';
import HeaderLogo from './HeaderLogo'
import MenuButton from '../menu/MenuButton'
import Menu from '../menu/Menu'

class Header extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isOpen: true,
      classN: ''
    };

    this.showMenu = this.showMenu.bind(this)
  }

  showMenu() {
    const classMenu = this.state.isOpen ? 'menu-is-open ' : '';
    this.setState({
      isOpen: !this.state.isOpen,
      classN: classMenu
    });
  }

  render() {
    return (
      <header>
        <div className={'container-fluid'}>
          <nav className={'navbar navbar-dark'}>
            <HeaderLogo/>
            <MenuButton onClick={this.showMenu}/>
            <Menu onClick={this.showMenu} res={this.state.classN}/>
          </nav>
        </div>
      </header>
    )
  }
}

export default Header