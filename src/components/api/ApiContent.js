import React from 'react'
import ReactAudioPlayer from 'react-audio-player';

class ApiContent extends React.Component {

  render() {
    return (
      <section id="api-content">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-12 z-i">
              <h3 className="track-title">{this.props.trackTitle}</h3>
              <div className="audio">
                <ReactAudioPlayer src={this.props.audio} autoPlay controls/>
              </div>
              <ul className="track-list">
                {this.props.respond}
              </ul>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

export default ApiContent