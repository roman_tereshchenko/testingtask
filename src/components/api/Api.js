import React from 'react'
import axios from 'axios'
import ApiComponent from './ApiContent'

class Api extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      respond: null,
      inputValue: '',
      audio: 'none',
      isPlay: true,
      trackTitle: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    let query = this.getUrl();
    axios.get(query)
      .then(respond => {
        let tracks = this.showTracks(respond.data.data);
        this.setState({
          respond: tracks
        })
      })
  }

  getUrl() {
    return 'https://cors-anywhere.herokuapp.com/https://api.deezer.com/search?q=' + this.state.inputValue
  }

  handleChange(e) {
    this.setState({inputValue: (e.target.value)})
  }

  showTracks(data) {

    return data.map((track) => <li key={data.id}>
      <div className="t wow bounceInDown" data-wow-delay="3s">
        <img id="img" src={track.album.cover_medium} alt=""/>
        <button className='buttonPlay' onClick={() => this.playTrack(track.preview, track.title)}>
        </button>
      </div>
      <div className="track-info">
        <h3>{track.title}</h3>
        <h5>{track.album.title}</h5>
      </div>
    </li>);

    /*return html*/
  }

  playTrack(sound, title) {
    this.setState({audio: sound, trackTitle: title, isPlay: false})
  }

  render() {
    return (
      <section id={'api'}>
        <div className='container-fluid'>
          <div className="row">
            <div className="col-md-12 z-i">
              <div className="row api-header justify-content-start align-items-center wow fadeInUp"
                   data-wow-delay="0.3s">
                <div className="api-logo"></div>
                <div className="api-search-bar d-flex">
                  <input className="form-control" id="api-search" type="text" value={this.state.inputValue}
                         onChange={this.handleChange}
                         placeholder="Enter artist name"/>
                  <button className="btn btn-default api-btn" onClick={this.handleClick}>send request</button>
                </div>
              </div>
            </div>
          </div>
          <ApiComponent respond={this.state.respond} trackTitle={this.state.trackTitle} audio={this.state.audio}/>
        </div>
      </section>
    )
  }
}

export default Api;