import React from 'react';
import ReactDOM from 'react-dom';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './animate.css';
import './fonts.css';
import './index.css';
import Main from './components/Main'

ReactDOM.render(
  <Main/>,
  document.getElementById('root')
);
